package me.speind.thefirstone;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import static android.app.ProgressDialog.show;
import static android.widget.Toast.makeText;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void click1 (View v){
        View buttview = findViewById(R.id.newbutt);
        buttview.setVisibility(View.VISIBLE);
        }

    public void click2 (View v){
        View buttview2 = findViewById(R.id.newbutt2);
        buttview2.setVisibility(View.VISIBLE);
        makeText(MainActivity.this, "button2 pressed", Toast.LENGTH_SHORT).show();
    }

    private void show() {
    }

}
