package me.speind.hangman;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


public class GameActivity extends Activity {
    String mWord;
    int mFailCounter = 0;
    int mGuessedLetters = 0;
    int mPoints = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        setRandomWord();
    }

    /**
     * The input field
     * @param v
     */
    public void introduceLetter(View v){
        EditText myEditText = (EditText) findViewById(R.id.editTextLetter);
        String letter = myEditText.getText().toString();
        myEditText.setText("");
        Log.d("MYLOG", "The letter is: " + letter);

        if (letter.length() == 1) {
            checkLetter(letter);
        } else {
            Toast.makeText(this,"No valid letter input",Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Checking letter matching
     *
     * @param introducedLetter = is the input letter
     */
    public void checkLetter(String introducedLetter){
        char charIntroduced = introducedLetter.charAt(0);
        boolean letterGuessed = false;
        for (int i =0 ; i < mWord.length(); i++){
            char charFromTheWord = mWord.charAt(i);
            if (charFromTheWord == charIntroduced){
                Log.d("MYLOG","There is a match");
                letterGuessed = true;
                showLettersAtIndex(i,charIntroduced);
                mGuessedLetters++;
            }
        }

        if (letterGuessed == false){
            letterFailed(Character.toString(charIntroduced));
        }

        if (mGuessedLetters == mWord.length()){
            mPoints++;
            clearScreen();
            setRandomWord();
        }
    }

    public void clearScreen(){
        TextView textViewFailed = (TextView) findViewById(R.id.textView7);
        textViewFailed.setText("");
        mGuessedLetters = 0;
        mFailCounter = 0;
        LinearLayout layoutLetters = (LinearLayout) findViewById(R.id.layoutLetters);
        for (int i=0; i < layoutLetters.getChildCount(); i++) {
            TextView currentTextView = (TextView) layoutLetters.getChildAt(i);
            currentTextView.setText("_");
        }

        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.hangdroid_0);
    }

    /**
     * Changing the hanging droid pic by fail counter
     */
    public void letterFailed(String letterFailed){
        TextView textViewFailed = (TextView) findViewById(R.id.textView7);
        String previousFail = textViewFailed.getText().toString();
        textViewFailed.setText(previousFail+letterFailed);

        mFailCounter++;
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        if (mFailCounter == 1) {
            imageView.setImageResource(R.drawable.hangdroid_1);
        }   else if (mFailCounter == 2){
            imageView.setImageResource(R.drawable.hangdroid_2);
        }   else if (mFailCounter == 3){
            imageView.setImageResource(R.drawable.hangdroid_3);
        }   else if (mFailCounter == 4){
            imageView.setImageResource(R.drawable.hangdroid_4);
        }   else if (mFailCounter == 5){
            imageView.setImageResource(R.drawable.hangdroid_5);
        }   else if (mFailCounter == 6){
            Intent gameOverIntent = new Intent(this,GameOverActivity.class);
            gameOverIntent.putExtra("POINTS_IDENTIFIER",mPoints);
            startActivity(gameOverIntent);
            Toast.makeText(this,"The correct word was "+mWord,Toast.LENGTH_SHORT).show();

        }
    }

    /**
     * Show guessed letters at their position
     * @param position the textView within four ones
     * @param letterGuessed the letter was guessed
     */
    public void showLettersAtIndex(int position, char letterGuessed){
        LinearLayout layoutLetter = (LinearLayout) findViewById(R.id.layoutLetters);
        TextView textView = (TextView) layoutLetter.getChildAt(position);
        textView.setText(Character.toString(letterGuessed));
    }

    /**
     * Setting a random word to guess
     */
    public void setRandomWord(){
        String words = "java able stop mind from work fart bold hurt dart that";
        String[] arrayWords = words.split(" ");
        int randomNumber = (int) (Math.random()*arrayWords.length);
        String randomWord=arrayWords[randomNumber];
        mWord=randomWord;
    }
}
